import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import GridList from '@material-ui/core/GridList';
import GridListTile from '@material-ui/core/GridListTile';
import GridListTileBar from '@material-ui/core/GridListTileBar';
import ListSubheader from '@material-ui/core/ListSubheader';
import withWidth, {isWidthUp} from '@material-ui/core/withWidth';
import Link from '@material-ui/core/Link';
import tileData from './tileData';


const useStyles = makeStyles(theme => ({
  root: {
    display: 'box',
    flexWrap: 'wrap',
    justifyContent: 'space-around',
    
    textAlign: 'center',
    height:'100',
    WebkitOverflowScrolling: 'touch', // Add iOS momentum scrolling.
  },
  tile:{
    position:'relative',
    display:'block',
    overflow: 'auto'
    
  },
  image:{
    height: '100%'
  },
  title: {
    fontSize: theme.typography.pxToRem(16),
    lineHeight: '30px',
    textOverflow: 'ellipsis',
    overflow: 'auto',
    whiteSpace: 'nowrap',
  },
  gridList: {
    width: '100',
    minHeight: 200,
    height: '100'
    
   
  },
  tileBar: {
    backgroundColor: 'rgba(0, 0, 0, 0.8)',
    textAlign: 'center',
    fontsize:'1vw'
  },
  tileTitle: {
    fontSize:'vw',
    marginBottom:'5px',
    color:'rgb(189, 222, 219)',
    display:'block'
  }
  
}));

const MyComponent = props => {
  const getGridListCols = () => {
    if (isWidthUp('xl', props.width)) {
      return 5;
    }

    if (isWidthUp('lg', props.width)) {
      return 3;
    }

    if (isWidthUp('md', props.width)) {
      return 2;
    }

    return 1;
  }

//export default function Apps() {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <GridList  cellHeight={200} className={classes.gridList} cols={getGridListCols()} spacing={15}>
        <GridListTile key="Subheader" className={classes.tile} cols={5} spacing={1} style={{ height: '' }}>
          <ListSubheader component="div">AFC Portal</ListSubheader>
        </GridListTile> 
        {tileData.map(tile => (
          <GridListTile key={tile.img} className={classes.tile} >
            <Link href={tile.href}><img className={classes.image} src={tile.img} alt={tile.title} /></Link>
          <Link href={tile.href}> <GridListTileBar className={classes.tileBar} title={tile.title} /> </Link>
          </GridListTile>
        ))}
      </GridList>
    </div>
  );
}

export default withWidth()(MyComponent);

