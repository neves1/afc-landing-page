import React from 'react';
import {
  Button,
  Jumbotron,
  Container } from 'reactstrap';
  import Apps from './Apps'

  

function WelcomeContent(props) {
  // If authenticated, greet the user
  if (props.isAuthenticated) {
    return (
      <Apps/>
    );
  }

  // Not authenticated, present a sign in button
  return( 
    <Jumbotron>
        <div class="text-center">
       <p><h1 className="title">Army Futures Command Landing Page</h1></p>
        </div>
        <div className="text-left">
          <p>YOU ARE ACCESSING A U.S. GOVERNMENT (USG) INFORMATION SYSTEM (IS) THAT IS PROVIDED FOR USG-AUTHORIZED USE ONLY.</p>
          <p>By using this IS (which includes any device attached to this IS), you consent to the following conditions:</p>
          <p>The USG routinely intercepts and monitors communications on this IS for purposes including, but not limited to, penetration testing, COMSEC monitoring, network operations and defense, personnel misconduct (PM), law enforcement (LE), and counterintelligence  (CI) investigations.</p>
          <p>At any time, the USG may inspect and seize data stored on this IS.</p>
          <p>Communications using, or data stored on, this IS are not private, are subject to routine monitoring, interception, and search, and may be disclosed or used for any USG-authorized purpose.</p>
          <p>This IS includes security measures (e.g., authentication and access controls) to protect USG interests--not for your personal benefit or privacy.</p>
          <p>Notwithstanding the above, using this IS does not constitute consent to PM, LE or CI investigative searching or monitoring of the content of privileged communications, or work product, related to personal representation or services by attorneys, psychotherapists, or clergy, and their assistants. Such communications and work product are private and confidential. See User Agreement for details.</p> 
        </div>
    <div class="text-center">
            <Button color="secondary" size="lg" onClick={props.authButtonMethod}>I Accept</Button>
    </div>
    </Jumbotron>
  )
}

export default class Welcome extends React.Component {
  render() {
    return (
      <Container>
        <div class="text-center">
        <WelcomeContent
          isAuthenticated={this.props.isAuthenticated}
          user={this.props.user}
          authButtonMethod={this.props.authButtonMethod} />
        </div>
      </Container>
    );
  }
}