
import React from "react";
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';

import org from './images/Org.png'


const routes = {
  orgcharts: "https://army.deps.mil/army/cmds/AFC/SitePages/Home.aspx#/org-charts",
  Boss: "https://army.deps.mil/army/cmds/AFC/SitePages/Home.aspx#/boss",
  helpfulLinks: 'https://army.deps.mil/army/cmds/AFC/SitePages/Home.aspx#/helpful-links',
  policies: "https://army.deps.mil/army/cmds/AFC/SitePages/Home.aspx/#/current-afc-policies",
  ccir: "https://army.deps.mil/army/cmds/AFC/SitePages/Home.aspx/#/ccirs",
  roster: "https://army.deps.mil/army/cmds/AFC/SitePages/Home.aspx/#/afc-hq-roster",
  floorplan: "https://army.deps.mil/army/cmds/AFC/SitePages/Home.aspx/#/floor-plans",
  battleRhythm: "https://army.deps.mil/army/cmds/AFC/SitePages/Home.aspx/#/battle-rhythm",
  templates: "https://army.deps.mil/army/cmds/AFC/SitePages/Home.aspx/#/template",
  congressionalTestimony: "https://army.deps.mil/army/cmds/AFC/SitePages/Home.aspx/#/congressional-testimony",
  afcTdas:"https://army.deps.mil/army/cmds/AFC/SitePages/Home.aspx/#/afc-tdas",
  whatsNew: "https://army.deps.mil/army/cmds/AFC/SitePages/Home.aspx/#/whats-new",
  howToSop: "https://army.deps.mil/army/cmds/AFC/SitePages/Home.aspx/#/how-tos-sops",
  lroc: "https://army.deps.mil/army/cmds/AFC/SitePages/FFME_Calendar.aspx",
  newToAfc: "https://army.deps.mil/army/cmds/AFC/SitePages/Home.aspx/#/new-to-afc"
}

const useStyles = makeStyles(theme => ({
  root: {
    
    
    
  },
  
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
    
  
  },
  
}));

export const Apps = (props) => {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <main className={classes.content}>
        <Grid container spacing={3}>
          <Grid item xs={3} >
            <Card>
              <CardActionArea component="a" href={routes.orgcharts}>
                <CardMedia
                  component="img"
                  height="300"
                  width="300"
                  image= {org}
                />
                <CardContent>
                  <Typography gutterBottom variant="h6" component="h2">
                    Organization Charts
                  </Typography>
                  <Typography variant="body2" color="textSecondary" component="p">
                  
                  </Typography>
                </CardContent>
              </CardActionArea>
              <CardActions>
                <Button size="small" color="secondary" component="a" href={routes.orgcharts}>
                  
                </Button>
              </CardActions>
            </Card>
          </Grid>
          <Grid item xs={6} sm={3} md={1}>
            <Card>
              <CardActionArea component="a" href={routes.Boss}>
                <CardMedia
                  component="img"
                  height="295"
                  image="BoSS.png"
                />
                <CardContent>
                  <Typography gutterBottom variant="h6" component="h2">
                    Book Some Space (BoSS)
                  </Typography>
                  <Typography variant="body2" color="textSecondary" component="p">
                   
                  </Typography>
                </CardContent>
              </CardActionArea>
              <CardActions>
                <Button size="small" color="secondary" component="a" href={routes.wekan}>
                  
                </Button>
              </CardActions>
            </Card>
          </Grid>
          <Grid item xs={6} sm={3} md={1}>
            <Card>
              <CardActionArea component="a" href={routes.helpfulLinks}>
                <CardMedia
                  component="img"
                  height="295"
                  image="Links.png"
                />
                <CardContent>
                  <Typography gutterBottom variant="h6" component="h2">
                    Helpful Links
                  </Typography>
                  <Typography variant="body2" color="textSecondary" component="p">
                    
                  </Typography>
                </CardContent>
              </CardActionArea>
              <CardActions>
                <Button size="small" color="secondary" component="a" href={routes.rocketChat}>
                  
                </Button>
              </CardActions>
            </Card>
          </Grid>
          <Grid item xs={6} sm={3} md={1}>
            <Card>
              <CardActionArea component="a" href={routes.policies}>
                <CardMedia
                  component="img"
                  height="295"
                  image="Policies.png"
                />
                <CardContent>
                  <Typography gutterBottom variant="h6" component="h2">
                    Current AFC Policies
                  </Typography>
                  <Typography variant="body2" color="textSecondary" component="p">
                    
                  </Typography>
                </CardContent>
              </CardActionArea>
              <CardActions>
                <Button size="small" color="secondary" component="a" href={routes.rocketChat}>
                  
                </Button>
              </CardActions>
            </Card>
          </Grid>
          <Grid item xs={6} sm={3} md={1}>
            <Card>
              <CardActionArea component="a" href={routes.ccir}>
                <CardMedia
                  component="img"
                  height="295"
                  image="CCIRs.png"
                />
                <CardContent>
                  <Typography gutterBottom variant="h6" component="h2">
                    CCIRs
                  </Typography>
                  <Typography variant="body2" color="textSecondary" component="p">
                    
                  </Typography>
                </CardContent>
              </CardActionArea>
              <CardActions>
                <Button size="small" color="secondary" component="a" href={routes.ccir}>
                  
                </Button>
              </CardActions>
            </Card>
          </Grid>
          <Grid item xs={6} sm={3} md={1}>
            <Card>
              <CardActionArea component="a" href={routes.roster}>
                <CardMedia
                  component="img"
                  height="295"
                  image="Roster.png"
                />
                <CardContent>
                  <Typography gutterBottom variant="h6" component="h2">
                    AFC HQ Roster
                  </Typography>
                  <Typography variant="body2" color="textSecondary" component="p">
                    
                  </Typography>
                </CardContent>
              </CardActionArea>
              <CardActions>
                <Button size="small" color="secondary" component="a" href={routes.roster}>
                  
                </Button>
              </CardActions>
            </Card>
          </Grid>
          <Grid item xs={6} sm={3} md={1}>
            <Card>
              <CardActionArea component="a" href={routes.floorplan}>
                <CardMedia
                  component="img"
                  height="295"
                  image="Floor.png"
                />
                <CardContent>
                  <Typography gutterBottom variant="h6" component="h2">
                    Floor Plans
                  </Typography>
                  <Typography variant="body2" color="textSecondary" component="p">
                    
                  </Typography>
                </CardContent>
              </CardActionArea>
              <CardActions>
                <Button size="small" color="secondary" component="a" href={routes.floorplan}>
                  
                </Button>
              </CardActions>
            </Card>
          </Grid>
          <Grid item xs={6} sm={3} md={1}>
            <Card>
              <CardActionArea component="a" href={routes.battleRhythm}>
                <CardMedia
                  component="img"
                  height="295"
                  image="Bat-Rhy.png"
                />
                <CardContent>
                  <Typography gutterBottom variant="h6" component="h2">
                    Battle Rhythm
                  </Typography>
                  <Typography variant="body2" color="textSecondary" component="p">
                    
                  </Typography>
                </CardContent>
              </CardActionArea>
              <CardActions>
                <Button size="small" color="secondary" component="a" href={routes.battleRhythm}>
                  
                </Button>
              </CardActions>
            </Card>
          </Grid>
          <Grid item xs={6} sm={3} md={1}>
            <Card>
              <CardActionArea component="a" href={routes.templates}>
                <CardMedia
                  component="img"
                  height="295"
                  image="Template.png"
                />
                <CardContent>
                  <Typography gutterBottom variant="h6" component="h2">
                    Templates
                  </Typography>
                  <Typography variant="body2" color="textSecondary" component="p">
                    
                  </Typography>
                </CardContent>
              </CardActionArea>
              <CardActions>
                <Button size="small" color="secondary" component="a" href={routes.templates}>
                  
                </Button>
              </CardActions>
            </Card>
          </Grid>
          <Grid item xs={6} sm={3} md={1}>
            <Card>
              <CardActionArea component="a" href={routes.congressionalTestimony}>
                <CardMedia
                  component="img"
                  height="295"
                  image="Testimonial.png"
                />
                <CardContent>
                  <Typography gutterBottom variant="h6" component="h2">
                    Congressional Testimony
                  </Typography>
                  <Typography variant="body2" color="textSecondary" component="p">
                    
                  </Typography>
                </CardContent>
              </CardActionArea>
              <CardActions>
                <Button size="small" color="secondary" component="a" href={routes.congressionalTestimony}>
                  
                </Button>
              </CardActions>
            </Card>
          </Grid>
          <Grid item xs={6} sm={3} md={1}>
            <Card>
              <CardActionArea component="a" href={routes.afcTdas}>
                <CardMedia
                  component="img"
                  height="295"
                  image="TDA.png"
                />
                <CardContent>
                  <Typography gutterBottom variant="h6" component="h2">
                    AFC TDAs
                  </Typography>
                  <Typography variant="body2" color="textSecondary" component="p">
                    
                  </Typography>
                </CardContent>
              </CardActionArea>
              <CardActions>
                <Button size="small" color="secondary" component="a" href={routes.afcTdas}>
                  
                </Button>
              </CardActions>
            </Card>
          </Grid>
          <Grid item xs={6} sm={3} md={1}>
            <Card>
              <CardActionArea component="a" href={routes.whatsNew}>
                <CardMedia
                  component="img"
                  height="295"
                  image="New.png"
                />
                <CardContent>
                  <Typography gutterBottom variant="h6" component="h2">
                    What's New on the Portal
                  </Typography>
                  <Typography variant="body2" color="textSecondary" component="p">
                    
                  </Typography>
                </CardContent>
              </CardActionArea>
              <CardActions>
                <Button size="small" color="secondary" component="a" href={routes.whatsNew}>
                  
                </Button>
              </CardActions>
            </Card>
          </Grid>
          <Grid item xs={6} sm={3} md={1}>
            <Card>
              <CardActionArea component="a" href={routes.howToSop}>
                <CardMedia
                  component="img"
                  height="295"
                  image="SOP.png"
                />
                <CardContent>
                  <Typography gutterBottom variant="h6" component="h2">
                    How TO's and SOPs
                  </Typography>
                  <Typography variant="body2" color="textSecondary" component="p">
                    
                  </Typography>
                </CardContent>
              </CardActionArea>
              <CardActions>
                <Button size="small" color="secondary" component="a" href={routes.howToSop}>
                  
                </Button>
              </CardActions>
            </Card>
          </Grid>
          <Grid item xs={6} sm={3} md={1}>
            <Card>
              <CardActionArea component="a" href={routes.lroc}>
                <CardMedia
                  component="img"
                  height="295"
                  image="Ops-Cal.png"
                />
                <CardContent>
                  <Typography gutterBottom variant="h5" component="h6">
                    AFC Long Range Operations Calendar
                  </Typography>
                  <Typography variant="body2" color="textSecondary" component="p">
                    
                  </Typography>
                </CardContent>
              </CardActionArea>
              <CardActions>
                <Button size="small" color="secondary" component="a" href={routes.lroc}>
                  
                </Button>
              </CardActions>
            </Card>
          </Grid>
          <Grid item xs={6} sm={3} md={1}>
            <Card>
              <CardActionArea component="a" href={routes.newToAfc}>
                <CardMedia
                  component="img"
                  height="295"
                  image="Onboard.png"
                />
                <CardContent>
                  <Typography gutterBottom variant="h6" component="h2">
                    New to AFC?
                  </Typography>
                  <Typography variant="body2" color="textSecondary" component="p">
                    
                  </Typography>
                </CardContent>
              </CardActionArea>
              <CardActions>
                <Button size="small" color="secondary" component="a" href={routes.newToAfc}>
                  
                </Button>
              </CardActions>
            </Card>
          </Grid>
        </Grid>
      </main>
    </div>
  );
}

export default Apps;