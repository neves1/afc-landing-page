import React from "react";
import { Route, Switch } from "react-router-dom";
import NotFound from "./ErrorMessage";
import Welcome from "./Welcome";


export default function Routes() {
  return (
    <Switch>
      <Route path="/" exact component={Home} />
      <Route path="/Welcome" exact component={Welcome} />
      { /* Finally, catch all unmatched routes */ }
      <Route component={NotFound} />
    </Switch>
  );
}