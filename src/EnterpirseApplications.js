import React from 'react';
import { Table } from 'reactstrap';
import moment from 'moment';
import config from './Config';
import { getapps } from './GraphService';

// Helper function to format Graph date/time
function formatDateTime(dateTime) {
  return moment.utc(dateTime).local().format('M/D/YY h:mm A');
}

export default class Calendar extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      apps: []
    };
  }

  async componentDidMount() {
    try {
      // Get the user's access token
      var accessToken = await window.msal.acquireTokenSilent({
        scopes: config.scopes
      });
      // Get the user's apps
      var apps = await getapps(accessToken);
      // Update the array of apps in state
      this.setState({apps: apps.value});
    }
    catch(err) {
      this.props.showError('ERROR', JSON.stringify(err));
    }
  }

  render() {
    return (
      <div>
        <h1>Enterprise Applications</h1>
        <Table>
          <thead>
            <tr>
              <th scope="col">Name</th>
              <th scope="col">Logo</th>
              <th scope="col">URL</th>
              
            </tr>
          </thead>
          <tbody>
            {this.state.apps.map(
              function(app){
                return(
                  <tr key={app.displayName}>
                    <td>{app.displayName}</td>
                    <td>{app.info.logoUrl}</td>
                    <td>{app.web.homePageUrl}</td>
                  </tr>
                );
              })}
          </tbody>
        </Table>
      </div>
    );
  }
}