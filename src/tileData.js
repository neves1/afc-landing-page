const tileData = [
    {
      img:'/static/images/Org.png',
      title: 'Organization Charts',
      href: "https://army.deps.mil/army/cmds/AFC/SitePages/Home.aspx#/org-charts"
    },
    {
      img: '/static/images/BoSS.png',
      title: 'Book Some Space (BoSS)',
      href: "https://army.deps.mil/army/cmds/AFC/SitePages/Home.aspx#/boss"
    },
    {
      img: '/static/images/Links.png',
      title: 'Helpful Links',
      href: 'https://army.deps.mil/army/cmds/AFC/SitePages/Home.aspx#/helpful-links'
    },
    {
      img: '/static/images/Policies.png',
      title: 'Current AFC Policies',
      href: "https://army.deps.mil/army/cmds/AFC/SitePages/Home.aspx/#/current-afc-policies"
      
    },
    {
      img: '/static/images/CCIRs.png',
      title: 'CCIRs',
      href:"https://army.deps.mil/army/cmds/AFC/SitePages/Home.aspx/#/ccirs"
    },
    {
      img: '/static/images/Roster.png',
      title: 'AFC HQ Roster',
      href: "https://army.deps.mil/army/cmds/AFC/SitePages/Home.aspx/#/afc-hq-roster"
    },
    {
      img: '/static/images/Floor.png',
      title: 'Floor Plans',
      href: "https://army.deps.mil/army/cmds/AFC/SitePages/Home.aspx/#/floor-plans"
    },
    {
      img: '/static/images/Bat-Rhy.png',
      title: 'Battle Rhythm',
      href: "https://army.deps.mil/army/cmds/AFC/SitePages/Home.aspx/#/battle-rhythm"
    },
    {
      img: '/static/images/Template.png',
      title: 'Templates',
      href: "https://army.deps.mil/army/cmds/AFC/SitePages/Home.aspx/#/template"
    },
    {
      img: '/static/images/Testimonial.png',
      title: 'Congressional Testimony',
      href: "https://army.deps.mil/army/cmds/AFC/SitePages/Home.aspx/#/congressional-testimony"
    },
    {
      img: '/static/images/TDA.png',
      title: 'AFC TDAs',
      href: "https://army.deps.mil/army/cmds/AFC/SitePages/Home.aspx/#/afc-tdas"
    },
    {
      img: '/static/images/New.png',
      title: "What's New on the Portal",
      href: 'https://army.deps.mil/army/cmds/AFC/SitePages/Home.aspx/#/whats-new'
    },
    {
      img: '/static/images/SOP.png',
      title: "How To's and SOPs",
      href: 'https://army.deps.mil/army/cmds/AFC/SitePages/Home.aspx/#/how-tos-sops'
    },
    {
      img: '/static/images/Ops-Cal.png',
      title: "AFC Long Range Operations Calendar",
      href: 'https://army.deps.mil/army/cmds/AFC/SitePages/FFME_Calendar.aspx'
    },
    {
      img: '/static/images/New.png',
      title: "New to AFC?",
      href: 'https://army.deps.mil/army/cmds/AFC/SitePages/Home.aspx/#/new-to-afc'
    },
  ];
  
  export default tileData;